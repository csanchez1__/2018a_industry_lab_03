package ictgradschool.industry.lab03.ex05;

/**
 * Created by anhyd on 3/03/2017.
 */
public class ExerciseFive {

    /**
     * Runs an example program.
     */
    private void start() {
        String letters = "X X O O X O X O X ";

        String row1 = getRow(letters, 1);

        String row2 = getRow(letters, 2);

        String row3 = getRow(letters, 3);

        printRows(row1, row2, row3);

        String leftDiagonal = getLeftDiagonal(row1, row2, row3);

        printDiagonal(leftDiagonal);
    }

    /**
     * TODO Implement this
     */
    public String getRow(String letters, int row) {

        String row1 ="";
        if (row > 1){
            row1 += letters.substring((row -1)*6, row*6);
        }else {
            row1 += letters.substring(0, row * 6);
        }
            return row1;
        //return null;
    }

    /**
     * TODO Implement this
     */
    public void printRows(String row1, String row2, String row3) {
        System.out.println(row1 + "\n" + row2 + "\n" + row3);
    }

    /**
     * TODO Implement this
     */
    public String getLeftDiagonal(String row1, String row2, String row3) {
        String diagonal;
        diagonal = row1.charAt(0) + " " + row2.charAt(2) + " " + row3.charAt(4);
        return diagonal;
//        return null;
    }

    /**
     * TODO Implement this
     */
    public void printDiagonal(String leftDiagonal) {
        System.out.println("Diagonal: " + leftDiagonal);
    }

    /**
     * Program entry point. Do not edit.
     */
    public void start1 () {
        String colours , first , second , third ;
        int position1 , position2 , position3 , length ;
        colours = "redorangeyellow" ;
        first = colours . substring ( 4 , 9 );
        second = colours . substring ( 0 , 4 );
        third = colours . charAt ( 0 ) + colours . substring ( 13 );
        length = third . length ();
        third = third . toUpperCase ();
        position1 = colours . indexOf ( 'A' );
        position2 = colours . indexOf ( "el" );
        position3 = colours . indexOf ( "or" );
        System . out . println ( "first: " + first );
        System . out . println ( "second: " + second );
        System . out . println ( "third: " + third );
        System . out . println ( "length: " + length );
        System . out . println ( "position1: " + position1 );
        System . out . println ( "position2: " + position2 );
        System . out . println ( "position3: " + position3 );
    }



    public static void main(String[] args) {
        ExerciseFive ex = new ExerciseFive();
        //ex.start1();
        ex.start();
    }
}
