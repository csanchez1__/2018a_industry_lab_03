package ictgradschool.industry.lab03;

import ictgradschool.Keyboard;

public class CalculateVolume {
    public void start() {
        double radius;
        System.out.println("\"Volume of a Sphere\"");
        System.out.println(" Enter the radius : ");
        radius = Double.parseDouble(Keyboard.readInput());
        double volume = (((4 * Math.PI) * Math.pow(radius, 3)) /3);
        System.out.println("Volume: " + volume);
    }


    public static void main(String[] args) {
        CalculateVolume calculateVolume = new CalculateVolume();
        calculateVolume.start();
    }

}
