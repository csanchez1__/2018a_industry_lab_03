package ictgradschool.industry.lab03.ex02;

import ictgradschool.Keyboard;

/**
 * Write a program that prompts the user to enter a range – 2 integers representing a lower bound and an upper bound.
 * You should use Keyboard.readInput() for this. Then, convert these bounds from String to int using Integer.parseInt().
 * Your program should then use Math.random() to generate 3 random integers that lie between the range entered (inclusive),
 * and then use Math.min() to determine which of the random integers is the smallest.
 */
public class ExerciseTwo {

    /**
     * TODO Your code here. You may also write additional methods if you like.
     */
    int lowerBound;
    int upperBound;

    public ExerciseTwo(){
        lowerBound = 0;
        upperBound = 0;
    }
    private void start() {
        int a, b, c;
        System.out.print("Lower Bound? ");
        lowerBound = Integer.parseInt(Keyboard.readInput());
        System.out.print("Upper Bound? ");
        upperBound = Integer.parseInt(Keyboard.readInput());
        a = (int)(Math.random() * ((upperBound - lowerBound)+1) + lowerBound);
        b = (int)(Math.random() * ((upperBound - lowerBound)+1) + lowerBound);
        c = (int)(Math.random() * ((upperBound - lowerBound)+1) + lowerBound);
        System.out.println("3 randomly generated numbers: " + a + " " + b + " " + c);
        System.out.println("Smallest number is " + Math.min(a, Math.min(b, c)));
    }

    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {

        ExerciseTwo ex = new ExerciseTwo();
        ex.start();

    }
}
