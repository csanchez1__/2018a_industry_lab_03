package ictgradschool.industry.lab03.ex04;

import com.sun.image.codec.jpeg.TruncatedFileException;
//import com.sun.org.apache.xpath.internal.operations.String;
import ictgradschool.Keyboard;
import javafx.scene.AmbientLight;

import java.lang.annotation.ElementType;

/**
 * Write a program that prompts the user to enter an amount and a number of decimal places.  The program should then
 * truncate the amount to the user-specified number of decimal places using String methods.
 *
 * <p>To truncate the amount to the user-specified number of decimal places, the String method indexOf() should be used
 * to find the position of the decimal point, and the method substring() should then be used to extract the amount to
 * the user-specified number of decimal places.  The program is to be written so that each task is in a separate method.
 * You need to write four methods, one method for each of the following tasks:</p>
 * <ul>
 *     <li>Printing the prompt and reading the amount from the user</li>
 *     <li>Printing the prompt and reading the number of decimal places from the user</li>
 *     <li>Truncating the amount to the user-specified number of DP's</li>
 *     <li>Printing the truncated amount</li>
 * </ul>
 */
public class ExerciseFour {
    //String a = new String();


    private void start() {

        // TODO Use other methods you create to implement this program's functionality.
        String amount = getAmount();

        int decimalPlace = getDecimalPlace();

        String truncatingNumber = truncateNum(amount, decimalPlace);

        printTruncateNumber(truncatingNumber, decimalPlace);
    }

    // TODO Write a method which prompts the user and reads the amount to truncate from the Keyboard
    public String getAmount(){
        System.out.print("Please enter an amount: ");
        String amount = Keyboard.readInput();
        return amount;
    }

    // TODO Write a method which prompts the user and reads the number of DP's from the Keyboard
    public int getDecimalPlace(){
        System.out.print("Please enter the number of decimal places: ");
        int decimalPlace = Integer.parseInt(Keyboard.readInput());
        return decimalPlace;
    }

    // TODO Write a method which truncates the specified number to the specified number of DP's
    public String truncateNum(String amount, int decimal){
        String save;
        save = amount;
        for(int x = 0; x <= save.length(); x++){
            //int i = 0;
            char c = save.charAt(x);
            x++;
            if(c == '.'){
                save = save.substring(0, x+decimal);
                return save;
            }
        }
        return save;
    }

    // TODO Write a method which prints the truncated amount
    public void printTruncateNumber(String truncatingNumber, int decimalPlace){
        System.out.println("Amount truncated to " + decimalPlace + " decimal places is: " + truncatingNumber);
    }
    /**
     * Program entry point. Do not edit.
     */
    public static void main(String[] args) {
        ExerciseFour ex = new ExerciseFour();
        ex.start();
    }
}
