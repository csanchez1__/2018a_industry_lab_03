package ictgradschool.industry.lab03.ex03;

import ictgradschool.Keyboard;

import java.util.Random;

/**
 * Write a program that prompts the user to enter a sentence, then prints out the sentence with a random character
 * missing. The program is to be written so that each task is in a separate method. See the comments below for the
 * different methods you have to write.
 */
public class ExerciseThree {
    char c;

    private void start() {

        String sentence = getSentenceFromUser();

        int randomPosition = getRandomPosition(sentence);

        printCharacterToBeRemoved(sentence, randomPosition);

        String changedSentence = removeCharacter(sentence, randomPosition);

        printNewSentence(changedSentence);
    }

    /**
     * Gets a sentence from the user.
     * @return
     */
    private String getSentenceFromUser() {

        // TODO Prompt the user to enter a sentence, then get their input.
        System.out.print("Enter a sentence: ");
        String sentence = Keyboard.readInput();
        return sentence;
        //return null;
    }

    /**
     * Gets an int corresponding to a random position in the sentence.
     */
    private int getRandomPosition(String sentence) {

        // TODO Use a combination of Math.random() and sentence.length() to get the desired result.
        Random random = new Random();
        //int a = random.nextInt(sentence.length());
        int a = (int)(Math.random() * (sentence.length() + 1));
        return a;
        //return -1;
    }

    /**
     * Prints a message stating the character to be removed, and its position.
     */
    private void printCharacterToBeRemoved(String sentence, int position) {

        // TODO Implement this method
        c = sentence.charAt(position);
//        sentence = sentence.replace(c, "");
        System.out.println("Removing " + c + " from position " + position);
    }

    /**
     * Removes a character from the given sentence, and returns the new sentence.
     */
    private String removeCharacter(String sentence, int position) {

        // TODO Implement this method
        //c = sentence.charAt(position);
        sentence = sentence.substring(0, position)+ "" + sentence.substring(position + 1);
        return sentence;
        //return null;

    }

    /**
     * Prints a message which shows the new sentence after the removal has occured.
     */
    private void printNewSentence(String changedSentence) {

        // TODO Implement this method
        System.out.println("New sentence is: " + changedSentence);
    }

    public static void main(String[] args) {
        ExerciseThree ex = new ExerciseThree();
        ex.start();
    }
}
